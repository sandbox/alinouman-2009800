<?php
//implementation of hook_form_alter
function helping_text_for_quiz_module_form_alter(&$form, &$form_state, $form_id)
{
	if($form_id=="quiz_question_answering_form"){
                        
			$nid=$form['question_nid']['#value'];
			$data=db_select('field_data_field_helping_text_data','f')->fields('f')
			->condition('f.entity_id',$nid)->execute()->fetchAll();
			$value=$data['0']->field_helping_text_data_value;
                        $markup='<h1>Helping Text</h1><br>'.$value;
                        $new=array('some_thing'=>array('#type'=>'item','#markup'=>$markup));
                        
                        $form=array_insert_before($form,'question',$new);
			
			
	}
}
function array_insert_before($array, $key, $new)
{
    $keys = array_keys($array);
    $pos = (int) array_search($key, $keys);
    return array_merge(
        array_slice($array, 0, $pos),
        $new,
        array_slice($array, $pos)
    );
}
?>